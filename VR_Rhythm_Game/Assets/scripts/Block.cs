using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum BlockColor
{
    Green,
    Red
}

public class Block : MonoBehaviour
{
    public BlockColor color;
    
    public GameObject brokenBlockLeft;
    public GameObject brokenBlockRight;
    public float brokenBlockForce;
    public float brokenBlockTorque;
    public float brokenBlockDestroyDelay;
    
    void OnTriggerEnter (Collider other)
    {
        if(other.CompareTag("swordRed"))
        {
            if(color == BlockColor.Red)
            {
                GameManager.instance.AddScore();
            }
            else
            {
                GameManager.instance.HitWrongBlock();
            }
            
            Hit();
        }
        else if(other.CompareTag("swordGreen"))
        {
            if(color == BlockColor.Green)
            {
                GameManager.instance.AddScore();
            }
            else
            {
                GameManager.instance.HitWrongBlock();
            }
            
            Hit();
        }
    }
    
    void Hit ()
    {
        // enable the broken pieces
        brokenBlockLeft.SetActive(true);
        brokenBlockRight.SetActive(true);
        
        // remove them as children
        brokenBlockLeft.transform.parent = null;
        brokenBlockRight.transform.parent = null;
        
        // add force to them
        Rigidbody leftRig = brokenBlockLeft.GetComponent<Rigidbody>();
        Rigidbody rightRig = brokenBlockRight.GetComponent<Rigidbody>();
        
        leftRig.AddForce(-transform.right * brokenBlockForce, ForceMode.Impulse);
        rightRig.AddForce(transform.right * brokenBlockForce, ForceMode.Impulse);
        
        // add torque to them
        leftRig.AddTorque(-transform.forward * brokenBlockForce, ForceMode.Impulse);
        rightRig.AddTorque(transform.forward * brokenBlockForce, ForceMode.Impulse);
        
        // destroy the broken pieces after a few seconds
        Destroy(brokenBlockLeft, brokenBlockDestroyDelay);
        Destroy(brokenBlockRight, brokenBlockDestroyDelay);
        
        // destroy the main block
        Destroy(gameObject);
    }
}